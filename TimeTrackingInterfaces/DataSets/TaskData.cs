﻿namespace TimeTrackingInterfaces.DataSets {
    public class TaskData {
        public long Id { get; set; }
        public string Name { get; set; }
        public int Hours { get; set; }

        public override string ToString() {
            return string.Format("{0} : {1}", Name, Hours);
        }
    }
}