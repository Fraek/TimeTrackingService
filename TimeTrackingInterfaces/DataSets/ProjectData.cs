﻿namespace TimeTrackingInterfaces.DataSets {
    public class ProjectData {
        public long Id { get; set; }
        public string Name { get; set; }
    }
}
