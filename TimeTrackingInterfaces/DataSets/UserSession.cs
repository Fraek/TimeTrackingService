﻿namespace TimeTrackingInterfaces.DataSets {
    public class UserSession {
        public long Id { get; set;  }
        public string Token { get; set; }
    }
}