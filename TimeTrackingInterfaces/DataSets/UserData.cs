﻿namespace TimeTrackingInterfaces.DataSets {
    public class UserData {
        public long Id { get; set; }
        public string Name { get; set; }
        public string Password { get; set; }
        public string Token { get; set; }
        public int ProjectId { get; set; }

        public override string ToString() {
            return string.Format("Id: {0}, Name: {1}, Password: {2}, Token: {3}, ProjectId: {4}", Id, Name, Password, Token, ProjectId);
        }
    }
}