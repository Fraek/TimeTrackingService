﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.ServiceModel;
using System.Text;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingInterfaces {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the interface name "IWCFTimeTrackerService" in both code and config file together.
    [ServiceContract]
    public interface IWCFTimeTrackerService {
        [OperationContract]
        List<string> ListTasks();

        [OperationContract]
        UserData Register(string username);

        [OperationContract]
        UserSession Login(string username, string password);

        [OperationContract]
        bool AddProject (string projectName, UserSession session);

        [OperationContract]
        ProjectData GetUserProject (UserSession session);

        [OperationContract]
        List<TaskData> GetProjectTaskListByString (string projectName, UserSession session);

        [OperationContract]
        List<TaskData> GetProjectTaskListByData (ProjectData projectData, UserSession session);

        [OperationContract]
        TaskData GetTaskDataByName (string name, UserSession session);

        [OperationContract]
        TaskData GetTaskDataById (long id, UserSession session);

        [OperationContract]
        bool AddHoursToTask (int hours, long taskId, UserSession session);

        [OperationContract]
        bool SaveTask (TaskData task, UserSession session);

        [OperationContract]
        bool DeleteTaskByName (string taskName, UserSession session);

        [OperationContract]
        bool DeleteTaskById (long id, UserSession session);

        [OperationContract]
        bool DeleteTask (TaskData taskData, UserSession session);

        [OperationContract]
        bool DeleteTimeTracking (TaskData taskData, UserSession session);
    }
}