﻿using System;
using System.Collections.Generic;
using System.ServiceModel;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingInterfaces {
    internal class Program {
        private static void Main(string[] args) {
            ChannelFactory<IWCFTimeTrackerService> channelFactory =
                new ChannelFactory<IWCFTimeTrackerService>("TimeTrackerServiceEndpoint");
            IWCFTimeTrackerService proxy = channelFactory.CreateChannel();

            string token = null;

            Console.WriteLine("Enter: taken | login | register | addProject | exit");
            string input = "";
            while (input != "exit") {
                input = Console.ReadLine();
                if (input == "taken") {
                    List<string> tasks = proxy.ListTasks();

                    foreach (var t in tasks) {
                        Console.WriteLine(t);
                    }
                }

                if (input == "login") {
                    Console.WriteLine("Vul username in:");
                    string username = Console.ReadLine();
                    Console.WriteLine("Vul wachtwoord in:");
                    string password = Console.ReadLine();

                    UserData loginInformation = proxy.Login(username, password);
                    if (loginInformation == null) {
                        Console.WriteLine("Ongeldige Login");
                    } else {
                        token = loginInformation.Token;
                        Console.WriteLine(loginInformation.ToString());
                    }
                }

                if (input == "register") {
                    Console.WriteLine("Vul username in:");
                    string username = Console.ReadLine();
                    UserData registrationData = proxy.Register(username);
                    Console.WriteLine(registrationData.ToString());
                }

                if (input == "addProject") {
                    if (token != null) {
                        Console.WriteLine("Vul projectnaam in:");
                        string projectName = Console.ReadLine();
                        bool successfull = proxy.AddProject(projectName);
                        Console.WriteLine(successfull);
                    } else {
                        Console.WriteLine("Je moet eerst inloggen!");
                    }
                }
            }
        }
    }
}