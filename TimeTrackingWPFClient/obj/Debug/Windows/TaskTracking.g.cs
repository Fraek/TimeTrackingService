﻿#pragma checksum "..\..\..\Windows\TaskTracking.xaml" "{406ea660-64cf-4c82-b6f0-42d48172a799}" "D3EC03FB54261201D59DA852DD360134"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.18408
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace TimeTrackingWPFClient.Windows {
    
    
    /// <summary>
    /// TaskTracking
    /// </summary>
    public partial class TaskTracking : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 11 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox TaskListBox;
        
        #line default
        #line hidden
        
        
        #line 12 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Label ProjectLabel;
        
        #line default
        #line hidden
        
        
        #line 14 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox TaskTextBox;
        
        #line default
        #line hidden
        
        
        #line 17 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBox HoursTextBox;
        
        #line default
        #line hidden
        
        
        #line 18 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.ListBox RegisteredTaskListBox;
        
        #line default
        #line hidden
        
        
        #line 21 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button AddHoursButton;
        
        #line default
        #line hidden
        
        
        #line 22 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button UpdateButton;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\Windows\TaskTracking.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Button DeleteButton;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/TimeTrackingWPFClient;component/windows/tasktracking.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\Windows\TaskTracking.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.TaskListBox = ((System.Windows.Controls.ListBox)(target));
            
            #line 11 "..\..\..\Windows\TaskTracking.xaml"
            this.TaskListBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.TaskListBox_SelectionChanged);
            
            #line default
            #line hidden
            
            #line 11 "..\..\..\Windows\TaskTracking.xaml"
            this.TaskListBox.LostFocus += new System.Windows.RoutedEventHandler(this.TaskListBox_OnLostFocus);
            
            #line default
            #line hidden
            return;
            case 2:
            this.ProjectLabel = ((System.Windows.Controls.Label)(target));
            return;
            case 3:
            this.TaskTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 4:
            this.HoursTextBox = ((System.Windows.Controls.TextBox)(target));
            return;
            case 5:
            this.RegisteredTaskListBox = ((System.Windows.Controls.ListBox)(target));
            
            #line 19 "..\..\..\Windows\TaskTracking.xaml"
            this.RegisteredTaskListBox.LostFocus += new System.Windows.RoutedEventHandler(this.RegisteredTaskListBox_OnLostFocus);
            
            #line default
            #line hidden
            
            #line 19 "..\..\..\Windows\TaskTracking.xaml"
            this.RegisteredTaskListBox.SelectionChanged += new System.Windows.Controls.SelectionChangedEventHandler(this.RegisteredTaskListBox_SelectionChanged);
            
            #line default
            #line hidden
            return;
            case 6:
            this.AddHoursButton = ((System.Windows.Controls.Button)(target));
            
            #line 21 "..\..\..\Windows\TaskTracking.xaml"
            this.AddHoursButton.Click += new System.Windows.RoutedEventHandler(this.AddHoursButtonButton_Click);
            
            #line default
            #line hidden
            return;
            case 7:
            this.UpdateButton = ((System.Windows.Controls.Button)(target));
            
            #line 22 "..\..\..\Windows\TaskTracking.xaml"
            this.UpdateButton.Click += new System.Windows.RoutedEventHandler(this.UpdateButton_Click);
            
            #line default
            #line hidden
            return;
            case 8:
            this.DeleteButton = ((System.Windows.Controls.Button)(target));
            
            #line 23 "..\..\..\Windows\TaskTracking.xaml"
            this.DeleteButton.Click += new System.Windows.RoutedEventHandler(this.DeleteButton_Click);
            
            #line default
            #line hidden
            return;
            }
            this._contentLoaded = true;
        }
    }
}

