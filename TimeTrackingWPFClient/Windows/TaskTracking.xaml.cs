﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using TimeTrackingInterfaces.DataSets;
using TimeTrackingWPFClient.Components;

namespace TimeTrackingWPFClient.Windows {
    /// <summary>
    /// Interaction logic for TaskTracking.xaml
    /// </summary>
    public partial class TaskTracking : Window {
        private int lastSelectedRegisteredTasksIndex = -1;

        public TaskTracking() {
            InitializeComponent();

            string ProjectName = ProjectComponent.getUserProjectName();
            ProjectLabel.Content = "Project: " + ProjectName;
            foreach(TaskData Task in TaskComponent.GetTaskDataList(ProjectName)) {
                TaskListBox.Items.Add(Task.Name);
                if (Task.Hours != 0) {
                    RegisteredTaskListBox.Items.Add(Task);
                }
            }
        }

        private void TaskListBox_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (TaskListBox.SelectedItem == null) {
                return;
            }
            TaskTextBox.Text = TaskListBox.SelectedItem.ToString();
            HoursTextBox.Text = "";
        }

        private void RegisteredTaskListBox_SelectionChanged (object sender, SelectionChangedEventArgs e) {
            if (RegisteredTaskListBox.SelectedItem == null) {
                return;
            }
            TaskData selectedTask = (TaskData)RegisteredTaskListBox.SelectedItem;
            TaskTextBox.Text = selectedTask.Name;
            HoursTextBox.Text = selectedTask.Hours.ToString();
        }

        private void UpdateButton_Click (object sender, RoutedEventArgs e) {
            if (!Regex.IsMatch(HoursTextBox.Text, @"^\d+$")) {
                MessageBox.Show("Please enter a valid hours-amount");
                return;
            }
            string taskName = TaskTextBox.Text;
            int hours = Convert.ToInt32(HoursTextBox.Text);
            TaskData taskData = null;

            foreach (TaskData task in RegisteredTaskListBox.Items) {
                if (task.Name == taskName) {
                    taskData = task;
                    task.Hours = hours;
                    break;
                }
            }

            //In case we are dealing with a task without timetracking
            if (taskData == null) {
                MessageBox.Show("Update only possible on Registered Tasks");
            }

            //This task has timetracking so lets add hours
            if (TaskComponent.updateTimeTracking(taskData)) {
                RegisteredTaskListBox.Items.Refresh();
            }
        }

        private void RegisteredTaskListBox_OnLostFocus(object sender, RoutedEventArgs e) {
            this.lastSelectedRegisteredTasksIndex = RegisteredTaskListBox.SelectedIndex;
            RegisteredTaskListBox.SelectedIndex = -1;
        }

        private void TaskListBox_OnLostFocus(object sender, RoutedEventArgs e) {
            TaskListBox.SelectedIndex = -1;
        }

        private void DeleteButton_Click (object sender, RoutedEventArgs e) {
            if (
                TaskComponent.DeleteTimeTracking(
                    (TaskData) RegisteredTaskListBox.Items[lastSelectedRegisteredTasksIndex])) {
                        RegisteredTaskListBox.Items.Remove(RegisteredTaskListBox.Items[lastSelectedRegisteredTasksIndex]);
                RegisteredTaskListBox.Items.Refresh();
            }
        }

        private void AddHoursButtonButton_Click(object sender, RoutedEventArgs e) {
            if (!Regex.IsMatch(HoursTextBox.Text, @"^\d+$")) {
                MessageBox.Show("Please enter a valid hours-amount");
                return;
            }
            string taskName = TaskTextBox.Text;
            int hours = Convert.ToInt32(HoursTextBox.Text);
            TaskData taskData = null;

            foreach (TaskData task in RegisteredTaskListBox.Items) {
                if (task.Name == taskName) {
                    taskData = task;
                    break;
                }
            }

            //In case we are dealing with a task without timetracking
            if (taskData == null) {
                taskData = TaskComponent.GetTaskData(taskName);
                if (TaskComponent.addHours(taskData, hours)) {
                    taskData.Hours = hours;
                    RegisteredTaskListBox.Items.Add(taskData);
                    RegisteredTaskListBox.Items.Refresh();
                    return;
                }
            }

            //This task has timetracking so lets add hours
            if (TaskComponent.addHours(taskData, hours)) {
                taskData.Hours += hours;
                RegisteredTaskListBox.Items.Refresh();
                return;
            }
        }
    }
}