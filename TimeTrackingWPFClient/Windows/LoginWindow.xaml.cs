﻿using System.Windows;

namespace TimeTrackingWPFClient.Windows {
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    public partial class LoginWindow : Window {
        public LoginWindow() {
            InitializeComponent();
        }

        private void Login_OnClick(object sender, RoutedEventArgs e) {
            string username = usernameTextBox.Text;
            string password = passwordTextBox.Password;
            if (!Components.UserComponent.Login(username, password)) {
                MessageBox.Show("Invalid Login");
                return;
            }
            
            new TaskTracking().Show();
            this.Close();
        }

        private void RegisterButton_OnClick(object sender, RoutedEventArgs e) {
            new RegisterWindow().Show();
            this.Close();
        }
    }
}