﻿using System;
using System.ServiceModel;
using TimeTrackingInterfaces;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingWPFClient.Components {
    public sealed class Proxy {
        private ChannelFactory<IWCFTimeTrackerService> channelFactory;
        private static IWCFTimeTrackerService serviceInstance;
        private static volatile Proxy proxyInstance;
        private static object syncRoot = new Object();
        private static UserSession UserSession = null;

        private Proxy() {
            channelFactory = new ChannelFactory<IWCFTimeTrackerService>("TimeTrackerServiceEndpoint");
            serviceInstance = channelFactory.CreateChannel();
        }

        public IWCFTimeTrackerService GetService() {
            return serviceInstance;
        }

        public void SetUserSession(UserSession session) {
            if (session == null) {
                throw new Exception("Provided User-Session can't be null");
            }

            UserSession = session;
        }

        public UserSession GetUserSession () {
            return UserSession;
        }

        public static Proxy Instance {
            get {
                if (proxyInstance == null) {
                    lock (syncRoot) {
                        if (proxyInstance == null) {
                            proxyInstance = new Proxy();
                        }
                    }
                }
                return proxyInstance;
            }
        }
    }
}