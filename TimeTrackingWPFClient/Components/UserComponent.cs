﻿using System;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingWPFClient.Components {
    public static class UserComponent {
        /// <summary>
        /// Does a login-attempt at the service. If successfull UserData is returned including a token.
        /// This token is stored in the proxy for later validation
        /// </summary>
        /// <param name="username"></param>
        /// <param name="password"></param>
        /// <returns>Boolean with the success state</returns>
        public static bool Login(string username, string password) {
            if (Proxy.Instance.GetUserSession() != null)
                throw new Exception("You are allready logged in");

            try {
                UserSession loginInformation = Proxy.Instance.GetService().Login(username, password);
                if (loginInformation == null) {
                    return false;
                }

                Proxy.Instance.SetUserSession(loginInformation);
            } catch {
                return false;
            }
            return true;
        }

        /// <summary>
        /// Registers the username and returns the generated password from the Service.
        /// </summary>
        /// <param name="username"></param>
        /// <returns>The generated password</returns>
        public static string Register(string username) {
            UserData registrationData = Proxy.Instance.GetService().Register(username);
            return registrationData.Password;
        }
    }
}