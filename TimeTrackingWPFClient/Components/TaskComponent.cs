﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Documents;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingWPFClient.Components {
    static class TaskComponent {
        public static List<TaskData> GetTaskDataList(string projectName) {
            if (Proxy.Instance.GetUserSession() == null)
                throw new Exception("Not logged in");

            List<TaskData> TaskDataList = Proxy.Instance.GetService().GetProjectTaskListByString(projectName, Proxy.Instance.GetUserSession());
            if (TaskDataList == null)
                throw new Exception("No task list found");

            return TaskDataList;
        }

        public static TaskData GetTaskData(string taskName) {
            if (Proxy.Instance.GetUserSession() == null)
                throw new Exception("Not logged in");

            return Proxy.Instance.GetService().GetTaskDataByName(taskName, Proxy.Instance.GetUserSession());
        }

        public static bool DeleteTask (TaskData taskData) {
            if (taskData == null) {
                throw new Exception("TaskData must be set");
            }
            Proxy.Instance.GetService().DeleteTask(
                taskData,
                Proxy.Instance.GetUserSession()
            );
            return true;
        }

        public static bool DeleteTimeTracking(TaskData taskData) {
            if (taskData == null) {
                throw new Exception("TaskData must be set");
            }
            Proxy.Instance.GetService().DeleteTimeTracking(
                taskData,
                Proxy.Instance.GetUserSession()
            );
            return true;
        }

        public static bool addHours (TaskData taskData, int hours) {
            if (taskData == null || taskData.Id <= 0) {
                throw new Exception("TaskData must be set");
            }
            return Proxy.Instance.GetService().AddHoursToTask(hours, taskData.Id, Proxy.Instance.GetUserSession());
        }

        public static bool updateTimeTracking (TaskData taskData) {
            if (taskData == null || taskData.Id <= 0) {
                throw new Exception("All TaskData must be set");
            }
            return Proxy.Instance.GetService().SaveTask(taskData, Proxy.Instance.GetUserSession());
        }

        
    }
}
