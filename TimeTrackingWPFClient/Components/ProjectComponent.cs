﻿using System;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingWPFClient.Components {
    public class ProjectComponent {
        public static string getUserProjectName() {
            ProjectData project = Proxy.Instance.GetService().GetUserProject(Proxy.Instance.GetUserSession());
            if (project == null) {
                throw new Exception("No project found");
            }
            return project.Name;
        } 
    }
}