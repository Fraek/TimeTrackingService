﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using TimeTrackingWPFClient.Components;
using TimeTrackingWPFClient.Windows;

namespace TimeTrackingWPFClient {
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application {
        private void Init(object sender, StartupEventArgs e) {
            LoginWindow wnd = new LoginWindow {Title = "TimeTracking WPF Client"};
            wnd.Show();

//            new TaskTracking().Show();
        }
    }
}