﻿using System;
using System.Collections.Generic;
using System.Linq;
using TimeTrackingInterfaces;
using TimeTrackingInterfaces.DataSets;

namespace TimeTrackingService {
    // NOTE: You can use the "Rename" command on the "Refactor" menu to change the class name "WCFTimeTrackerService" in both code and config file together.
    public class WCFTimeTrackerService : IWCFTimeTrackerService {
        public List<string> ListTasks() {
            Console.WriteLine("ListTasks has been called by a client");

            List<string> tasksList = new List<string>();
            try {
                using (timetrackerEntities database = new timetrackerEntities()) {
                    var tasks = from t in database.tasks
                        select t.name;

                    tasksList = tasks.ToList();
                }
            } catch {}

            return tasksList;
        }

        public UserData Register(string username) {
            Console.WriteLine("Register has been called by a client");

            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    string generatedPassword = this.generatePassword(username);
                    user newUser = new user {
                        name = username,
                        password = generatedPassword,
                        projectId = 1
                    };

                    database.users.Add(newUser);
                    database.SaveChanges();
                    return new UserData {
                        Id = newUser.id,
                        Password = newUser.password,
                        Name = newUser.name,
                        Token = newUser.token
                    };
                } catch {
                    return null;
                }
            }
        }

        public TimeTrackingInterfaces.DataSets.UserSession Login (string username, string password) {
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    user user = (from usr in database.users
                        where usr.name == username &&
                              usr.password == password
                        select usr).First<user>();
                    user = setRandomToken(user);
                    return new TimeTrackingInterfaces.DataSets.UserSession{
                        Id = user.id,
                        Token = user.token
                    };
                } catch {
                    return null;
                }
            }
        }

        public bool AddProject (string projectName, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    database.projects.Add(
                        new project {
                            name = projectName
                        });
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        public ProjectData GetUserProject(TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return null;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    project project = ( from prj in database.projects
                                        join usr in database.users
                                        on prj.id equals usr.projectId
                                        select prj).First<project>();
                    return new ProjectData {
                        Id = project.id,
                        Name = project.name
                       };
                } catch {
                    return null;
                }
            }
        }

        public List<TaskData> GetProjectTaskListByData (ProjectData projectData, TimeTrackingInterfaces.DataSets.UserSession session) {
            return GetProjectTaskListByString(projectData.Name, session);
        }

        public List<TaskData> GetProjectTaskListByString(string projectName, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return null;
            }
           
            try {
                using (timetrackerEntities database = new timetrackerEntities()) {
                    var query = from prjtsks in database.projecttasks
                        join tsks in database.tasks
                            on prjtsks.taskId equals tsks.id
                        join prj in database.projects
                            on prjtsks.projectId equals prj.id
                        join timetrck in database.timetrackings
                            on tsks.id equals timetrck.taskId into timeTrackedList
                            from timetrck in timeTrackedList.DefaultIfEmpty()
                        where prj.name == projectName
                        select new {Task = tsks, TimeTracking = timetrck != null ? timetrck : null};

                    List<TaskData> tasksDataList = new List<TaskData>();
                    foreach (var result in query) {
                        int hours = 0;
                        if (result.TimeTracking != null) {
                            hours = result.TimeTracking.ammount ?? default(int);
                        }

                        tasksDataList.Add(
                            new TaskData {
                                Id = result.Task.id,
                                Name = result.Task.name,
                                Hours = hours
                            }
                        );
                    
                    }

                    return tasksDataList;
                }
            } catch {
                return null;
            }
        }

        public TaskData GetTaskDataByName(string name, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return null;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    TaskData taskData = (   from tsk in database.tasks
                                            join timetrack in database.timetrackings
                                            on tsk.id equals timetrack.taskId into taskDataList
                                            from timetrack in taskDataList.DefaultIfEmpty()
                                            where tsk.name == name
                                            select new TaskData { Hours = timetrack.ammount ?? default(int) , Id = tsk.id, Name = tsk.name }).Single<TaskData>();
                    return taskData;
                } catch {
                    return null;
                }
            }
        }

        public TaskData GetTaskDataById (long id, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return null;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    TaskData taskData = (from tsk in database.tasks
                                         join timetrack in database.timetrackings
                                         on tsk.id equals timetrack.taskId into taskDataList
                                         from timetrack in taskDataList.DefaultIfEmpty()
                                         where tsk.id == id
                                         select new TaskData { Hours = timetrack.ammount ?? default(int), Id = tsk.id, Name = tsk.name }).Single<TaskData>();
                    return taskData;
                } catch {
                    return null;
                }
            }
        }

        public bool AddHoursToTask(int hours, long taskId, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    timetracking databaseTimetracking = (from dbTaskTracking in database.timetrackings
                                                         where dbTaskTracking.userId == session.Id
                                                         && dbTaskTracking.taskId == taskId
                                                         select dbTaskTracking).FirstOrDefault<timetracking>();

                    if (databaseTimetracking != null) {
                        databaseTimetracking.ammount += hours;
                        database.SaveChanges();
                        return true;
                    }

                    database.timetrackings.Add(
                        new timetracking {
                            userId = session.Id,
                            ammount = hours,
                            taskId = taskId
                        });
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        public bool SaveTask(TaskData task, TimeTrackingInterfaces.DataSets.UserSession session) {
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    timetracking databaseTimetracking = (   from dbTaskTracking in database.timetrackings
                                            where task.Id == dbTaskTracking.taskId
                                            && session.Id == dbTaskTracking.userId
                                            select dbTaskTracking).Single<timetracking>();

                    if (databaseTimetracking != null) {
                        databaseTimetracking.ammount = task.Hours;
                        databaseTimetracking.userId = session.Id;
                        database.SaveChanges();
                        return true;
                    }
                    return false;
                } catch {
                    return false;
                }
            }
        }

        public bool DeleteTaskByName(string taskName, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    task task = (from tsk in database.tasks
                                         where tsk.name == taskName
                                         select tsk).Single<task>();
                    database.tasks.Remove(task);
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        public bool DeleteTaskById(long id, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    task task = (from tsk in database.tasks
                                 where tsk.id == id
                                 select tsk).Single<task>();
                    database.tasks.Remove(task);
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        public bool DeleteTask(TaskData taskData, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    task task = (from tsk in database.tasks
                                 where tsk.id == taskData.Id
                                 && tsk.name == taskData.Name
                                 select tsk).Single<task>();
                    database.tasks.Remove(task);
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        public bool DeleteTimeTracking(TaskData taskData, TimeTrackingInterfaces.DataSets.UserSession session) {
            if (!UserSession.IsValid(session)) {
                return false;
            }
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    timetracking timetracking = (   from tt in database.timetrackings
                                                    where tt.taskId == taskData.Id
                                                    && tt.userId == session.Id
                                                    select tt).Single<timetracking>();
                    database.timetrackings.Remove(timetracking);
                    database.SaveChanges();
                    return true;
                } catch {
                    return false;
                }
            }
        }

        private string generatePassword(string username) {
            char[] charArray = username.ToCharArray();
            Array.Reverse(charArray);
            return new string(charArray);
        }

        private user setRandomToken(user _user) {
            using (timetrackerEntities database = new timetrackerEntities()) {
                user user = (from usr in database.users
                    where usr.name == _user.name
                    select usr).First<user>();
                string randomToken = this.getRandomToken();
                user.token = randomToken;
                database.SaveChanges();
                return user;
            }
        }

        private string getRandomToken() {
            Guid g = Guid.NewGuid();
            string GuidString = Convert.ToBase64String(g.ToByteArray());
            return g.ToString();
        }
    }
}