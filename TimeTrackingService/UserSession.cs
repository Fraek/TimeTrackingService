﻿using System.Linq;

namespace TimeTrackingService {
    public class UserSession : TimeTrackingInterfaces.DataSets.UserSession{

        public UserSession(TimeTrackingInterfaces.DataSets.UserSession session) {
            Id = session.Id;
            Token = session.Token;
        }

        public static bool IsValid(TimeTrackingInterfaces.DataSets.UserSession session) {
            UserSession userSession = new UserSession(session);
            return userSession.IsValid();
        }

        public bool IsValid() {
            using (timetrackerEntities database = new timetrackerEntities()) {
                try {
                    user user = (from usr in database.users
                                where usr.id == Id
                                && usr.token == Token
                                select usr).First<user>();
                    return user != null;
                } catch {
                    return false;
                }
            }
        }
    }
}
